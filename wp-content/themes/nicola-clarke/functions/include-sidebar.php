<?php
	function tastic_register_sidebars() {
		register_sidebar( array(
			'name' => 'Sidebar',
			'id' => 'blog-sidebar',
			'before_widget' => '<section class="widget">',
			'after_widget' => '</section></section>',
			'before_title' => '<h2>',
			'after_title' => '</h2><section class="widget-body">',
		) );
		register_sidebar( array(
			'name' => 'Latest Products',
			'id' => 'latest-products',
			'before_widget' => '<section class="latest-products">',
			'after_widget' => '</section>',
			'before_title' => '<h2 class="feature-title">',
			'after_title' => '</h2>',
		) );
		register_sidebar( array(
			'name' => 'Shop Categories',
			'id' => 'shop-cats',
			'before_widget' => '<section class="widget">',
			'after_widget' => '</section>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );
	}
?>
