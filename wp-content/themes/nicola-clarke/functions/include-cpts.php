<?php
	function tastic_posts() {
		/*
			register_post_type(
				'publications',
				array(
					'labels'		=> array (
										'name'					=> _x('Publications', 'post type general name'),
										'singular_name'			=> _x('Publication', 'post type singular name'),
										'add_new'				=> _x('Add new Publication', 'book'),
										'add_new_item'			=> ('Add New Publication'),
										'edit_item'				=> ('Edit Publication'),
										'new_item'				=> ('New Publication'),
										'all_items'				=> ('All Publications'),
										'view_item'				=> ('View Publications'),
										'search_items'			=> ('Search Publications'),
										'not_found'				=> ('No Publications found'),
										'not_found_in_trash'	=> ('No Publications found in trash'),
										'parent_item_colon'		=> '',
										'menu_name'				=> 'Publications'
									),
					'description'	=> 'Holds all the Publications',
					'public'		=> true,
					'menu_position'	=> 19,
					'menu_icon'		=> 'dashicons-book-alt',
					'supports'		=> array('title', 'editor', 'excerpt', 'revisions', 'thumbnail'),
					'has_archive'	=> true
				)
			);
		*/
	}
	function tastic_taxonomies() {
		/*
			register_taxonomy( 
				'publication-type', 
				'publications', 
				array(
		            'labels' 		=> array(
							 			'name'              => _x( 'Publication Type', 'taxonomy general name' ),
							            'singular_name'     => _x( 'Publication Type', 'taxonomy singular name' ),
							            'search_items'      => __( 'Search Publication Types' ),
							            'all_items'         => __( 'All Publication Types' ),
							            'parent_item'       => __( 'Parent Publication Type' ),
							            'parent_item_colon' => __( 'Parent Publication Type:' ),
							            'edit_item'         => __( 'Edit Publication Type' ), 
							            'update_item'       => __( 'Update Publication Type' ),
							            'add_new_item'      => __( 'Add New Publication Type' ),
							            'new_item_name'     => __( 'New Publication Type' ),
							            'menu_name'         => __( 'Publication Types' ),
							        ),
		            'hierarchical' => true,
		        )
			);
		*/
    }
?>
