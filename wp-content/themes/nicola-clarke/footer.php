	<footer>
		<section class="container">
			<?php if(!is_page_template('template-holding.php')) { ?>
				<nav class="main">
					<?php wp_nav_menu( array('theme_location' => 'header-menu') ); ?>
					<ul class="socials">
						<li>
							<a href="https://twitter.com/n_c_design" target="_blank"><i class="fa fa-twitter-square"></i></a>
						</li>
						<li>
							<a href="https://www.facebook.com/pages/Nicola-Clarke-Jewellery/307499049300059" target="_blank"><i class="fa fa-facebook-square"></i></a>
						</li>
						<li>
							<a href="https://instagram.com/nicolaclarkedesigns" target="_blank"><i class="fa fa-instagram"></i></a>
						</li>
						<li>
							<a href="#"><i class="fa fa-envelope-square"></i></a>
						</li>
					</ul>
				</nav>
			<?php } ?>
			<section class="footer-content">
				<aside class="footer-main">
					<p>Nicola Clarke is a designer maker, creating adornments, for the body, home and weddings, inspired by her love of nature, tattoos and her extensive travels around the world working on community and conservation projects.</p>
					<?php echo do_shortcode('[contact-form-7 id="2453" title="Mailing List Sign Up"]'); ?>
					<p><span>Website content &copy; Nicola Clarke <?php echo date('Y'); ?></span> <span class="pipe">|</span> <span>Website by <a href="http://wedo.digital" target="_blank">wedo.digital</a></span></p>
				</aside>
				<aside class="footer-logo">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="&copy; Nicola Clarke <?php the_time('Y'); ?>">
				</aside>
			</section>
		</section>
	</footer>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/js/font-awesome/css/font-awesome.min.css">
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-35663434-8', 'auto');
	  ga('send', 'pageview');

	</script>
	<?php wp_footer(); ?>
</body>
</html>
