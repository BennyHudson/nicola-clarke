<?php

	add_action( 'after_setup_theme', 'tastic_theme_setup' );
	function tastic_theme_setup() {
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_action( 'widgets_init', 'tastic_register_sidebars' );
		add_filter('wp_mail_from_name', 'new_mail_from_name');
		add_action( 'init', 'register_my_menus' );
		add_action( 'login_enqueue_scripts', 'my_login_logo' );
		add_action('login_head', 'add_favicon');
		add_action('admin_head', 'add_favicon');
		add_action('init', 'tastic_posts');
		add_action('init', 'tastic_taxonomies');
		add_action('init', 'flush_rewrite_rules');
		add_action( 'wp_enqueue_scripts', 'tastic_scripts' );
		add_theme_support( 'woocommerce' );
		add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
		add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 20;' ), 20 );
		add_image_size( 'gallery-thumb', 400, 400, true );
	}

	function new_mail_from_name($old) {
		$blogname = get_bloginfo('name');
		return $blogname;
	}

	add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
	  function jk_related_products_args( $args ) {
		$args['posts_per_page'] = 5; // 4 related products
		$args['columns'] = 2; // arranged in 2 columns
		return $args;
	}

	get_template_part('functions/include', 'favicons');
	get_template_part('functions/include', 'postnav');
	get_template_part('functions/include', 'menus');
	get_template_part('functions/include', 'scripts');
	get_template_part('functions/include', 'sidebar');
	get_template_part('functions/include', 'cpts');

?>
