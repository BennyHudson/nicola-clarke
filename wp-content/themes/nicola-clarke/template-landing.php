<?php
	//Template Name: Landing Page
	get_header();
?>

	<section class="container ultra no-top">

		<!--Feature-->
		<section class="home-section">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/feature.png" alt="Nicola Clarke - Hand Crafted Ethical Silver Jewellery, Ceramics &amp; Sketches">
		</section>

		<!--Products-->
		<section class="home-section">
			<?php dynamic_sidebar( 'latest-products' ); ?>
		</section>

		<section class="home-section">
			<aside class="home-news">
				<h2 class="feature-title">From The Blog</h2>
				<?php 
					$postslist = get_posts('numberposts=3');
				    foreach ($postslist as $post) {
				?>
					<?php setup_postdata($post); ?>
						<aside class="excerpt">
							<?php get_template_part('includes/partial', 'excerpt'); ?>
						</aside>
					<?php wp_reset_postdata(); ?>	
				<?php } ?>
			</aside>
			<aside class="home-sidebar">
				<div class="widget">
					<a href="<?php bloginfo('url'); ?>/getting-wed/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/get-wed.png" alt="Get Wed"></a>
				</div>
				<div class="gallery">
					<h2 class="feature-title"><i class="fa fa-instagram"></i> Instagram</h2>

					<?php echo do_shortcode('[instagram-feed]'); ?>

					<!--
						<h2 class="feature-title">Gallery</h2>
						<?php
							$rows = get_field('gallery', 18);
							$first_row = $rows[0];
							$first_row_image = $first_row['image'];
							$second_row = $rows[1];
							$second_row_image = $second_row['image'];
							$third_row = $rows[2];
							$third_row_image = $third_row['image'];
							$fourth_row = $rows[3];
							$fourth_row_image = $fourth_row['image'];
							$fifth_row = $rows[4];
							$fifth_row_image = $fifth_row['image'];
							$sixth_row = $rows[5];
							$sixth_row_image = $sixth_row['image'];
							$seventh_row = $rows[6];
							$seventh_row_image = $seventh_row['image'];
							$eighth_row = $rows[7];
							$eighth_row_image = $eighth_row['image'];
							$ninth_row = $rows[8];
							$ninth_row_image = $ninth_row['image'];

							// Note
							// $first_row_image = 123 (image ID)

							$first_image = wp_get_attachment_image_src( $first_row_image, 'thumbnail' );
							$second_image = wp_get_attachment_image_src( $second_row_image, 'thumbnail' );
							$third_image = wp_get_attachment_image_src( $third_row_image, 'thumbnail' );
							$fourth_image = wp_get_attachment_image_src( $fourth_row_image, 'thumbnail' );
							$fifth_image = wp_get_attachment_image_src( $fifth_row_image, 'thumbnail' );
							$sixth_image = wp_get_attachment_image_src( $sixth_row_image, 'thumbnail' );
							$seventh_image = wp_get_attachment_image_src( $seventh_row_image, 'thumbnail' );
							$eighth_image = wp_get_attachment_image_src( $eighth_row_image, 'thumbnail' );
							$ninth_image = wp_get_attachment_image_src( $ninth_row_image, 'thumbnail' );
						?>
						<ul class="three-wide">
							<li><a href="<?php echo get_the_permalink(18); ?>"><img src="<?php echo $first_image[0]; ?>" alt="<?php the_title(); ?>" /></a></li>
							<li><a href="<?php echo get_the_permalink(18); ?>"><img src="<?php echo $second_image[0]; ?>" alt="<?php the_title(); ?>" /></a></li>
							<li><a href="<?php echo get_the_permalink(18); ?>"><img src="<?php echo $third_image[0]; ?>" alt="<?php the_title(); ?>" /></a></li>
							<li><a href="<?php echo get_the_permalink(18); ?>"><img src="<?php echo $fourth_image[0]; ?>" alt="<?php the_title(); ?>" /></a></li>
							<li><a href="<?php echo get_the_permalink(18); ?>"><img src="<?php echo $fifth_image[0]; ?>" alt="<?php the_title(); ?>" /></a></li>
							<li><a href="<?php echo get_the_permalink(18); ?>"><img src="<?php echo $sixth_image[0]; ?>" alt="<?php the_title(); ?>" /></a></li>
							<li><a href="<?php echo get_the_permalink(18); ?>"><img src="<?php echo $seventh_image[0]; ?>" alt="<?php the_title(); ?>" /></a></li>
							<li><a href="<?php echo get_the_permalink(18); ?>"><img src="<?php echo $eighth_image[0]; ?>" alt="<?php the_title(); ?>" /></a></li>
							<li><a href="<?php echo get_the_permalink(18); ?>"><img src="<?php echo $ninth_image[0]; ?>" alt="<?php the_title(); ?>" /></a></li>
						</ul>

					-->
				</div>
			</aside>
		</section>

	</section>

<?php get_footer(); ?>
