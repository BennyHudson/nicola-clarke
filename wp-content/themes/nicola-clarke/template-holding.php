<?php 
	// Template Name: Holding Page
	get_header(); 
?>

	<section class="container ultra holding">
		<h2>Changes are coming...</h2>
		<p>I'm making some changes behind the scenes and will be back soon.</p>
		<p>In the mean time, find me on <a href="https://www.instagram.com/nicolaclarkedesign/" target="_blank">Instagram</a> or <a href="https://twitter.com/n_c_design" target="_blank">Twitter</a></p>
	</section>

<?php get_footer(); ?>
