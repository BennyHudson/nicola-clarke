<div class="post-excerpt">
	<aside class="thumbnail">
		<?php
			$rows = get_field('gallery' ); // get all the rows
			$first_row = $rows[0]; // get the first row
			$first_row_image = $first_row['image' ]; // get the sub field value 

			// Note
			// $first_row_image = 123 (image ID)

			$image = wp_get_attachment_image_src( $first_row_image, 'gallery-thumb' );
		?>
		<a href="<?php the_permalink(); ?>">
			<?php if($rows) { ?>
				<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" />
			<?php } else { ?>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/blog-placeholder.png" alt="<?php the_title(); ?>">
			<?php } ?>
		</a>
	</aside>
	<aside class="content">
		<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<?php the_excerpt(); ?>
		<div class="post-meta">
			<span><i class="fa fa-calendar"></i> <?php the_time('F j, Y'); ?></span> <span><i class="fa fa-tags"></i> <?php the_category( ', ' ); ?></span> <a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
		</div>
	</aside>
</div>
