<?php if(is_page()) { ?>
    <?php
        if($post->post_parent)
        $children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
            else
            $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
            if ($children) { ?>
    	<section class="widget subnav">
            <a href="#" class="subnav-trigger"><?php the_title(); ?></a>
            <ul class="subnav">
                <?php echo $children; ?>
                <?php if($post->post_parent == 60) { ?>
                	<li><a href="<?php bloginfo('url'); ?>/add-listing/?listing_type=gd_place">Sign Up</a></li>
                <?php } ?>
            </ul>
    	</section>
    <?php } ?>
<?php } ?>
<?php 
    get_post_type();
    if ( 'post' == get_post_type() ) { 
        dynamic_sidebar( 'blog-sidebar' );
    }
?>
