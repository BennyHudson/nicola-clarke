<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/jquery-1.8.2.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/modernizr-custom.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.addListener.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/enquire.min.js" type="text/javascript"></script>
<link href='https://fonts.googleapis.com/css?family=Arvo:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width"/>  
<?php get_template_part('includes/include', 'favicon'); ?>
<title><?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
<div id="fb-root"></div>
</head>

<?php 
	if(!is_front_page()) {
		$front_page_id = get_option( 'page_on_front' );
		if(($front_page_id == 2374) && !is_user_logged_in()) {
			wp_redirect(get_bloginfo('url'));
		}
	}
?>

<body <?php body_class('nicola-clarke'); ?>>
	<section class="page-content">

		<div class="full-overlay"></div>
		<div class="overlay-wrap" id="contact-overlay">
			<div class="overlay-container">
				<div class="overlay-content contact">
					<a href="#" class="overlay-close"><i class="fa fa-times-circle fa-2x"></i></a>
					<h2 class="page-title">Get In Touch</h2>
					<div class="clear"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus a lobortis metus. Nullam est velit, dignissim ut tortor luctus, volutpat consectetur eros. Mauris cursus, dui in feugiat tincidunt, purus urna tristique felis, pulvinar fringilla erat leo a diam. Ut feugiat eros a dapibus varius. Nulla congue et enim eu tincidunt. Morbi eget faucibus mi. Donec sit amet dignissim sem. Cras efficitur suscipit sodales.</p>
					<?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
				</div>
			</div>
		</div>

		<?php if(!is_page_template('template-holding.php')) { ?>

			<?php get_template_part('snippets/nav', 'mobile'); ?>

		<?php } ?>

		<section class="footer-fix">
			<header class="site-header">
				<section class="container">
					<?php if(is_front_page()) { ?>
						<h1><a href="<?php bloginfo('url'); ?>"><span class="skull-left"><span class="skull-right">Nicola Clarke</span></span></a></h1>
					<?php } else { ?>
						<h2><a href="<?php bloginfo('url'); ?>"><span class="skull-left"><span class="skull-right">Nicola Clarke</span></span></a></h2>
					<?php } ?>
					<?php if(!is_page_template('template-holding.php')) { ?>
						<nav class="main">
							<?php wp_nav_menu( array('theme_location' => 'header-menu') ); ?>
						</nav>
					<?php } ?>
				</section>
			</header>
