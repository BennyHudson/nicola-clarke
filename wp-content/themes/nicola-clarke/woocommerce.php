<?php get_header(); ?>

	<section class="shop-note">
		<section class="container">
			<p>For jewellery or illustration commissions, or enquiries about wedding services or interior work please contact <a href="mailto:hello@nicola-clarke.co.uk">hello@nicola-clarke.co.uk</a> or use the <a href="<?php bloginfo('url'); ?>/about/contact">contact form</a>.</p>
			<p>For every item sold through this store a contribution will be made to the Orangutan Project, helping to fund wildlife rehabilitation and conservation. For more information on our partners in Borneo please see our <a href="<?php bloginfo('url'); ?>/about/faq/">FAQ</a>. </p>
		</section>
	</section>

	<section class="container ultra no-top">
		<div class="shop-nav">
			<?php wp_nav_menu( array('theme_location' => 'shop-menu') ); ?>
		</div>
		<?php if(is_archive()) { ?>
			<?php dynamic_sidebar( 'shop-cats' ); ?>
		<?php } ?>
		<?php woocommerce_content(); ?>
	</section>

<?php get_footer(); ?>
