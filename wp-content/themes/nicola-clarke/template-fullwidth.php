<?php 
	//Template Name: Full Width
	get_header(); 
?>

	<section class="container ultra">
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<h1 class="page-title"><?php the_title(); ?></h1>
				<div class="shop-nav">
					<?php wp_nav_menu( array('theme_location' => 'shop-menu') ); ?>
				</div>
				<?php the_post_thumbnail('full'); ?>
				<section class="page-body">
					<?php the_content(); ?>
					<?php if(is_page(11)) { ?>
						<a href="#" class="back-top">Back to Top</a>
					<?php } ?>
				</section>
			<?php endwhile; ?>
		<?php else: ?>
            <?php get_template_part('partials/template', 'error'); ?>
        <?php endif; ?>
	</section>

<?php get_footer(); ?>
