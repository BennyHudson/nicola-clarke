<?php 
	//Template Name: Gallery
	get_header(); 
?>

	<section class="container ultra">
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<h1 class="page-title"><?php the_title(); ?></h1>
				
				<section class="page-body">
					<?php the_content(); ?>
					<?php if(get_field('gallery')) { ?>
						<section class="gallery">
							<ul id="gallery-thumbs" class="seven-wide">
								<?php while(the_repeater_field('gallery')) { ?>
									<?php $thumb = wp_get_attachment_image_src(get_sub_field('image'), 'thumbnail' ); ?>
									<li><a href="#"><img src="<?php echo $thumb[0];?>" alt="<?php if(get_sub_field('title')) { the_sub_field('title'); } else { the_title(); } ?>"></a></li>
								<?php } ?>
							</ul>
						</section>
						<div class="overlay-wrap" id="gallery-content">
							<div class="overlay-container">
								<div class="overlay-content">
									<a href="#" class="overlay-close"><i class="fa fa-times-circle fa-2x"></i></a>
									<ul id="gallery-images">
										<?php while(the_repeater_field('gallery')) { ?>
											<?php
												$full = wp_get_attachment_image_src(get_sub_field('image'), 'large' );
											?>
											<li><img src="<?php echo $full[0];?>" alt="<?php if(get_sub_field('title')) { the_sub_field('title'); } else { the_title(); } ?>" title="<?php the_sub_field('description'); ?>"></li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					<?php } ?>
				</section>
			<?php endwhile; ?>
		<?php else: ?>
            <?php get_template_part('partials/template', 'error'); ?>
        <?php endif; ?>
	</section>

<?php get_footer(); ?>
