<?php get_header(); ?>

	<section class="container ultra">
		<aside class="page-sidebar post-sidebar">
			<?php get_sidebar(); ?>
		</aside>
		<aside class="page-main">
			<?php if ( have_posts() ) : ?>
				<h1 class="page-title">Blog</h1>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part('includes/partial', 'excerpt'); ?>
				<?php endwhile; ?>
			<?php else: ?>
	            <?php get_template_part('partials/template', 'error'); ?>
	        <?php endif; ?>
	        <section class="pagination">
	        	<?php wpbeginner_numeric_posts_nav(); ?>
	        </section>
		</aside>
	</section>

<?php get_footer(); ?>
