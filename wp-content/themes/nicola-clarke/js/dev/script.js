$(document).ready(function() {	

    var footerHeight = $('footer').outerHeight();
    $('.footer-fix').css('padding-bottom', footerHeight);

    $('.mobile-nav-trigger').click(function(e) {
        e.preventDefault();
        $('.mobile-nav').slideToggle();
        $(this).toggleClass('active');
    });

    $('.overlay-close').click(function(e) {
        e.preventDefault();
        $('html').css('overflow', 'auto');
        $('.full-overlay, .overlay-wrap').hide();
    });

    $('#gallery-thumbs li a').click(function(e) {
        e.preventDefault();
        $('html').css('overflow', 'hidden');
        $('.full-overlay, #gallery-content').show().addClass('visible');
    });

    $('.contact-trigger a').click(function(e) {
        e.preventDefault();
        $('html').css('overflow', 'hidden');
        $('.full-overlay, #contact-overlay').show().addClass('visible');
    })

    $('#gallery-thumbs').find('li a').each( function(i){
        var postnum = i; // remove any possible overloaded "+" operator ambiguity
        $(this).attr('data-slide-index', postnum);
    });

    $('.full-overlay, #gallery-content').show();
    var buzzSlider;
    window.buzzSliderWrapper = jQuery('#gallery-images').bxSlider({
        pagerCustom: '#gallery-thumbs',
        pager: true,
        useCSS: false,
        speed: 0,
        adaptiveHeight: true,
        captions: true,
        nextText: '<i class="fa fa-chevron-right"></i>',
        prevText: '<i class="fa fa-chevron-left"></i>',
        onSliderLoad: function(){
            buzzSlider = this;
            $('.full-overlay, #gallery-content').hide();   
        },
        onSlideAfter: function($slideElement, oldIndex, newIndex){
            this.speed = 500;   
            
            // Update red border on the thumbnails
            $('#gallery-images li').removeClass('active');
            
            var newSlide = $('#gallery-images li')[newIndex];
            $(newSlide).addClass('active');
        }
    });

    $('.woocommerce .images, .woocommerce .summary, .product > .onsale').wrapAll('<section class="product-top" />');

    $('.subnav-trigger').click(function(e) {
        e.preventDefault();
        $(this).next('ul').slideToggle();
        $(this).toggleClass('active');
    });

});

enquire
    .register("screen and (min-width:31em)", function() { 
        $(document).ready(function() {
               
        });
    }, true)
    .register("screen and (max-width:31em)", function() { 
        $(document).ready(function() {
            
            jQuery('.home-section .product_list_widget').bxSlider({
                minSlides: 2,
                maxSlides: 2,
                moveSlides: 1,
                slideWidth: 400,
                slideMargin: 10,
                controls: false
            })
            
        });
    });
